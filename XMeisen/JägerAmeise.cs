using AntMe.Deutsch;

namespace AntMe.Player.XMeisen
{
    public class JägerAmeise : StandardAmeise
    {
        public JägerAmeise(XMeisen masterMeise) : base(masterMeise)
        {
        }



        public override void Wartet()
        {
            GeheGeradeaus(30);
        }

        public override void RiechtFreund(Markierung markierung)
        {
            var mark = (Markierungen)markierung.Information;
            switch (mark)
            {
                case Markierungen.WanzeAngreifen:
                    break;
                case Markierungen.AmeiseAngreifen:

                    break;
                case Markierungen.WanzeInReichweite:
                    if(AnzahlAmeisenDerSelbenKasteInSichtweite > 3 && !(Ziel is Wanze))
                        GeheZuZiel(markierung);
                    break;
                case Markierungen.AmeiseInReichweite:
                    if(AnzahlAmeisenDerSelbenKasteInSichtweite > 1 && !(Ziel is Ameise))
                        GeheZuZiel(markierung);
                    break;
            }
        }

        public override void SiehtFeind(Ameise ameise)
        {
            SprüheMarkierung(Markierungen.AmeiseAngreifen, MarkierungsGröße.AmeiseAngreifen);
            GreifeAn(ameise);
        }

        public override void SiehtFeind(Wanze wanze)
        {
            SprüheMarkierung(Markierungen.WanzeInReichweite, MarkierungsGröße.WanzeInReichweite);
        }

        public override void WirdAngegriffen(Ameise ameise)
        {
            SprüheMarkierung(Markierungen.AmeiseAngreifen, MarkierungsGröße.AmeiseAngreifen);

            if (AnzahlAmeisenDerSelbenKasteInSichtweite > 1)
                GreifeAn(ameise);
        }

        public override void WirdAngegriffen(Wanze wanze)
        {
            if (AnzahlAmeisenDerSelbenKasteInSichtweite > 3)
            {
                SprüheMarkierung(Markierungen.WanzeAngreifen, MarkierungsGröße.WanzeAngreifen);
                GreifeAn(wanze);
            }
            else
            {
                SprüheMarkierung(Markierungen.WanzeInReichweite, MarkierungsGröße.WanzeInReichweite);
                GeheWegVon(wanze, Sichtweite - 5);
            }
        }
    }
}