using AntMe.Deutsch;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace AntMe.Player.XMeisen
{
    /// <summary>
    /// Diese Datei enthält die Beschreibung für deine Ameise. Die einzelnen Code-Blöcke 
    /// (Beginnend mit "public override void") fassen zusammen, wie deine Ameise in den 
    /// entsprechenden Situationen reagieren soll. Welche Befehle du hier verwenden kannst, 
    /// findest du auf der Befehlsübersicht im Wiki (http://wiki.antme.net/de/API1:Befehlsliste).
    /// 
    /// Wenn du etwas Unterstützung bei der Erstellung einer Ameise brauchst, findest du
    /// in den AntMe!-Lektionen ein paar Schritt-für-Schritt Anleitungen.
    /// (http://wiki.antme.net/de/Lektionen)
    /// 
    /// Kasten stellen "Berufsgruppen" innerhalb deines Ameisenvolkes dar. Du kannst hier mit
    /// den Fähigkeiten einzelner Ameisen arbeiten. Wie genau das funktioniert kannst du der 
    /// Lektion zur Spezialisierung von Ameisen entnehmen (http://wiki.antme.net/de/Lektion7).
    /// </summary>
    [Spieler(
        Volkname = "XMeisen",
        Vorname  = "Steffen",
        Nachname = "Vogel"
    )]
    [Kaste(
        Name = KasteStandard,
        AngriffModifikator = 0, 
        DrehgeschwindigkeitModifikator = 0,
        EnergieModifikator = 0,
        GeschwindigkeitModifikator = 0,
        LastModifikator = 0,
        ReichweiteModifikator = 0,
        SichtweiteModifikator = 0
    )]
    [Kaste(
        Name = KasteSammler,
        AngriffModifikator = -1, 
        DrehgeschwindigkeitModifikator = -1,
        EnergieModifikator = 0,
        GeschwindigkeitModifikator = 1,
        LastModifikator = 2,
        ReichweiteModifikator = 0,
        SichtweiteModifikator = -1
    )]
    [Kaste(
        Name = KasteJäger,
        AngriffModifikator = 1,
        DrehgeschwindigkeitModifikator = 0,
        EnergieModifikator = 1,
        GeschwindigkeitModifikator = 1,
        LastModifikator = -1,
        ReichweiteModifikator = -1,
        SichtweiteModifikator = -1
    )]
    [Kaste(
        Name = KasteKämpfer,
        AngriffModifikator = 2,
        DrehgeschwindigkeitModifikator = -1,
        EnergieModifikator = 2,
        GeschwindigkeitModifikator = 0,
        LastModifikator = -1,
        ReichweiteModifikator = -1,
        SichtweiteModifikator = -1
    )]
    [Kaste(
        Name = KasteSpäher,
        AngriffModifikator = 0,
        DrehgeschwindigkeitModifikator = 0,
        EnergieModifikator = 0,
        GeschwindigkeitModifikator = 0,
        LastModifikator = 0,
        ReichweiteModifikator = 0,
        SichtweiteModifikator = 0
    )]
    public class XMeisen : Basisameise
    {
        private AmeisenBasis _ameise;

        protected const string KasteStandard = "Standard";
        protected const string KasteSammler  = "Sammler";
        protected const string KasteJäger    = "Jäger";
        protected const string KasteKämpfer  = "Kämpfer";
        protected const string KasteSpäher   = "Späher";

        #region Kasten

        /// <summary>
        /// Jedes mal, wenn eine neue Ameise geboren wird, muss ihre Berufsgruppe
        /// bestimmt werden. Das kannst du mit Hilfe dieses Rückgabewertes dieser 
        /// Methode steuern.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:BestimmeKaste
        /// </summary>
        /// <param name="anzahl">Anzahl Ameisen pro Kaste</param>
        /// <returns>Name der Kaste zu der die geborene Ameise gehören soll</returns>
        public override string BestimmeKaste(Dictionary<string, int> anzahl)
        {
            var anzahlStandard = anzahl.ContainsKey(KasteStandard) ? anzahl[KasteStandard] : 0;
            var anzahlSammler  = anzahl.ContainsKey(KasteSammler)  ? anzahl[KasteSammler]  : 0;
            var anzahlJäger    = anzahl.ContainsKey(KasteJäger)    ? anzahl[KasteJäger]    : 0;
            var anzahlKämpfer  = anzahl.ContainsKey(KasteKämpfer)  ? anzahl[KasteKämpfer]  : 0;
            var anzahlSpäher   = anzahl.ContainsKey(KasteSpäher)   ? anzahl[KasteSpäher]   : 0;

            var ameisenGesamt = anzahlStandard + anzahlSammler + anzahlJäger + anzahlKämpfer + anzahlSpäher;
                ameisenGesamt = ameisenGesamt > 0 ? ameisenGesamt : 1;
            
            Debug.Print($@"
                        anzahlStandard = {anzahlStandard}
                        anzahlSammler  = {anzahlSammler}
                        anzahlJäger    = {anzahlJäger}
                        anzahlKämpfer  = {anzahlKämpfer}
                        anzahlSpäher   = {anzahlSpäher}
                        ");

            if (ameisenGesamt % 5 == 0)
            {
                Debug.Print($"Erzeuge Ameise {KasteSpäher}, {anzahlSpäher} vorhanden");
                _ameise = new SpäherAmeise(this);
                return KasteJäger;
            }

            if (ameisenGesamt % 6 == 0)
            {
                Debug.Print($"Erzeuge Ameise {KasteKämpfer}, {anzahlKämpfer} vorhanden");
                _ameise = new JägerAmeise(this);
                return KasteJäger;
            }

            if (ameisenGesamt % 4 == 0)
            {
                Debug.Print($"Erzeuge Ameise {KasteJäger}, {anzahlJäger} vorhanden");
                _ameise = new JägerAmeise(this);
                return KasteJäger;
            }
            
            Debug.Print($"Erzeuge Ameise {KasteSammler} {anzahlSammler} vorhanden");
            _ameise = new SammlerAmeise(this);
            return KasteSammler;
        }

        #endregion

        #region Fortbewegung

        /// <summary>
        /// Wenn die Ameise keinerlei Aufträge hat, wartet sie auf neue Aufgaben. Um dir das 
        /// mitzuteilen, wird diese Methode hier aufgerufen.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:Wartet
        /// </summary>
        public override void Wartet()
        {
            _ameise.Wartet();
        }

        /// <summary>
        /// Erreicht eine Ameise ein drittel ihrer Laufreichweite, wird diese Methode aufgerufen.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:WirdM%C3%BCde
        /// </summary>
        public override void WirdMüde()
        {
            _ameise.WirdMüde();
        }

        /// <summary>
        /// Wenn eine Ameise stirbt, wird diese Methode aufgerufen. Man erfährt dadurch, wie 
        /// die Ameise gestorben ist. Die Ameise kann zu diesem Zeitpunkt aber keinerlei Aktion 
        /// mehr ausführen.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:IstGestorben
        /// </summary>
        /// <param name="todesart">Art des Todes</param>
        public override void IstGestorben(Todesart todesart)
        {
            _ameise.IstGestorben(todesart);
        }

        /// <summary>
        /// Diese Methode wird in jeder Simulationsrunde aufgerufen - ungeachtet von zusätzlichen 
        /// Bedingungen. Dies eignet sich für Aktionen, die unter Bedingungen ausgeführt werden 
        /// sollen, die von den anderen Methoden nicht behandelt werden.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:Tick
        /// </summary>
        public override void Tick()
        {
            _ameise.Tick();
        }

        #endregion

        #region Nahrung

        /// <summary>
        /// Sobald eine Ameise innerhalb ihres Sichtradius einen Apfel erspäht wird 
        /// diese Methode aufgerufen. Als Parameter kommt das betroffene Stück Obst.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:Sieht(Obst)"
        /// </summary>
        /// <param name="obst">Das gesichtete Stück Obst</param>
        public override void Sieht(Obst obst)
        {
            _ameise.Sieht(obst);
        }

        /// <summary>
        /// Sobald eine Ameise innerhalb ihres Sichtradius einen Zuckerhügel erspäht wird 
        /// diese Methode aufgerufen. Als Parameter kommt der betroffene Zuckerghügel.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:Sieht(Zucker)"
        /// </summary>
        /// <param name="zucker">Der gesichtete Zuckerhügel</param>
        public override void Sieht(Zucker zucker)
        {
            _ameise.Sieht(zucker);
        }

        /// <summary>
        /// Hat die Ameise ein Stück Obst als Ziel festgelegt, wird diese Methode aufgerufen, 
        /// sobald die Ameise ihr Ziel erreicht hat. Ab jetzt ist die Ameise nahe genug um mit 
        /// dem Ziel zu interagieren.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:ZielErreicht(Obst)"
        /// </summary>
        /// <param name="obst">Das erreichte Stück Obst</param>
        public override void ZielErreicht(Obst obst)
        {
            _ameise.ZielErreicht(obst);
        }

        /// <summary>
        /// Hat die Ameise eine Zuckerhügel als Ziel festgelegt, wird diese Methode aufgerufen, 
        /// sobald die Ameise ihr Ziel erreicht hat. Ab jetzt ist die Ameise nahe genug um mit 
        /// dem Ziel zu interagieren.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:ZielErreicht(Zucker)"
        /// </summary>
        /// <param name="zucker">Der erreichte Zuckerhügel</param>
        public override void ZielErreicht(Zucker zucker)
        {
            _ameise.ZielErreicht(zucker);
        }

        #endregion

        #region Kommunikation

        /// <summary>
        /// Markierungen, die von anderen Ameisen platziert werden, können von befreundeten Ameisen 
        /// gewittert werden. Diese Methode wird aufgerufen, wenn eine Ameise zum ersten Mal eine 
        /// befreundete Markierung riecht.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:RiechtFreund(Markierung)"
        /// </summary>
        /// <param name="markierung">Die gerochene Markierung</param>
        public override void RiechtFreund(Markierung markierung)
        {
            _ameise.RiechtFreund(markierung);
        }

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel erspähen können, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus dem eigenen Volk, so 
        /// wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFreund(Ameise)"
        /// </summary>
        /// <param name="ameise">Erspähte befreundete Ameise</param>
        public override void SiehtFreund(Ameise ameise)
        {
            _ameise.SiehtFreund(ameise);
        }

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel erspähen können, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus einem befreundeten Volk 
        /// (Völker im selben Team), so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtVerb%C3%BCndeten(Ameise)"
        /// </summary>
        /// <param name="ameise">Erspähte verbündete Ameise</param>
        public override void SiehtVerbündeten(Ameise ameise)
        {
            _ameise.SiehtVerbündeten(ameise);
        }

        #endregion

        #region Kampf

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel erspähen können, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus einem feindlichen Volk, 
        /// so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFeind(Ameise)"
        /// </summary>
        /// <param name="ameise">Erspähte feindliche Ameise</param>
        public override void SiehtFeind(Ameise ameise)
        {
            _ameise.SiehtFeind(ameise);
        }

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel erspähen können, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Wanze, so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFeind(Wanze)"
        /// </summary>
        /// <param name="wanze">Erspähte Wanze</param>
        public override void SiehtFeind(Wanze wanze)
        {
            _ameise.SiehtFeind(wanze);
        }

        /// <summary>
        /// Es kann vorkommen, dass feindliche Lebewesen eine Ameise aktiv angreifen. Sollte 
        /// eine feindliche Ameise angreifen, wird diese Methode hier aufgerufen und die 
        /// Ameise kann entscheiden, wie sie darauf reagieren möchte.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:WirdAngegriffen(Ameise)"
        /// </summary>
        /// <param name="ameise">Angreifende Ameise</param>
        public override void WirdAngegriffen(Ameise ameise)
        {
            _ameise.WirdAngegriffen(ameise);
        }

        /// <summary>
        /// Es kann vorkommen, dass feindliche Lebewesen eine Ameise aktiv angreifen. Sollte 
        /// eine Wanze angreifen, wird diese Methode hier aufgerufen und die Ameise kann 
        /// entscheiden, wie sie darauf reagieren möchte.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:WirdAngegriffen(Wanze)"
        /// </summary>
        /// <param name="wanze">Angreifende Wanze</param>
        public override void WirdAngegriffen(Wanze wanze)
        {
            _ameise.WirdAngegriffen(wanze);
        }

        #endregion
    }
}
