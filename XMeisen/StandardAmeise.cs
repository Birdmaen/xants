using AntMe.Deutsch;

namespace AntMe.Player.XMeisen
{
    public class StandardAmeise : AmeisenBasis
    {
        public StandardAmeise(XMeisen masterMeise) : base(masterMeise)
        {
        }

        public override void Wartet()
        {
            SucheNeuesZiel();
        }

        public override void WirdMüde()
        {
            if(MussZumBauSonstVerhungert())
                GeheZuBau();
        }

        public override void IstGestorben(Todesart todesart)
        {
            if(todesart == Todesart.Verhungert)
                Denke("Bin verhungert :(");
            if (todesart == Todesart.Gefressen)
                Denke("Bin gefressen worden :(");
            if (todesart == Todesart.Besiegt)
                Denke("Bin getötet worden :(");
        }

        public override void Tick()
        {
            if (MussZumBauSonstVerhungert())
                GeheZuBau();
        }

        public override void Sieht(Obst obst)
        {
            SprüheMarkierung(Markierungen.ObstInReichweite, MarkierungsGröße.ObstInReichweite);
        }

        public override void Sieht(Zucker zucker)
        {
            SprüheMarkierung(Markierungen.ZuckerInReichweite, MarkierungsGröße.ZuckerInReichweite);
        }

        public override void ZielErreicht(Obst obst)
        {
            
        }

        public override void ZielErreicht(Zucker zucker)
        {
            
        }

        public override void RiechtFreund(Markierung markierung)
        {
            
        }

        public override void SiehtFreund(Ameise ameise)
        {

        }

        public override void SiehtVerbündeten(Ameise ameise)
        {
            
        }

        public override void SiehtFeind(Ameise ameise)
        {
            SprüheMarkierung(Markierungen.AmeiseInReichweite, MarkierungsGröße.AmeiseInReichweite);
        }

        public override void SiehtFeind(Wanze wanze)
        {
            SprüheMarkierung(Markierungen.WanzeInReichweite, MarkierungsGröße.WanzeInReichweite);
        }

        public override void WirdAngegriffen(Ameise ameise)
        {
            SprüheMarkierung(Markierungen.AmeiseAngreifen, MarkierungsGröße.AmeiseAngreifen);
            GeheWegVon(ameise, 15);
        }

        public override void WirdAngegriffen(Wanze wanze)
        {
            SprüheMarkierung(Markierungen.WanzeAngreifen, MarkierungsGröße.WanzeAngreifen);
            GeheWegVon(wanze, 15);
        }
    }
}