using System;
using AntMe.Deutsch;
using AntMe.Simulation;

namespace AntMe.Player.XMeisen
{
    public abstract class AmeisenBasis
    {
        protected XMeisen MasterMeise;

        protected const string KasteStandard = "Standard";
        protected const string KasteSammler  = "Sammler";
        protected const string KasteJ�ger    = "J�ger";
        protected const string KasteSp�her   = "Sp�her";

        protected AmeisenBasis(XMeisen masterMeise)
        {
            MasterMeise = masterMeise;
        }
        
        public Obst GetragenesObst => MasterMeise.GetragenesObst;
        public Spielobjekt Ziel => MasterMeise.Ziel;
        public bool IstM�de => MasterMeise.IstM�de;
        public bool Angekommen => MasterMeise.Angekommen;
        public int Drehgeschwindigkeit => MasterMeise.Drehgeschwindigkeit;

        public int EntfernungZumBau     => MasterMeise.EntfernungZuBau;
        public int Reichweite           => MasterMeise.Reichweite;
        public int Zur�ckgelegteStrecke => MasterMeise.Zur�ckgelegteStrecke;
        public int Sichtweite => MasterMeise.Sichtweite;
        
        public int AktuelleLast => MasterMeise.AktuelleLast;
        public int MaximaleLast => MasterMeise.MaximaleLast;
        public int AktuelleEnergie => MasterMeise.AktuelleEnergie;
        public int MaximaleEnergie => MasterMeise.MaximaleEnergie;
        public int MaximaleGeschwindigkeit => MasterMeise.MaximaleGeschwindigkeit;
        public int AktuelleGeschwindigkeit => MasterMeise.AktuelleGeschwindigkeit;

        public int AnzahlAmeisenDerSelbenKasteInSichtweite => MasterMeise.AnzahlAmeisenDerSelbenKasteInSichtweite;
        public int AnzahlAmeisenDesTeamsInSichtweite       => MasterMeise.AnzahlAmeisenDesTeamsInSichtweite;
        public int AnzahlAmeisenInSichtweite               => MasterMeise.AnzahlAmeisenInSichtweite;
        public int AnzahlFremderAmeisenInSichtweite        => MasterMeise.AnzahlFremderAmeisenInSichtweite;
        public int AnzahlWanzenInSichtweite                => MasterMeise.WanzenInSichtweite;

        #region Hilfsmethoden

        public bool MussZumBauSonstVerhungert()
        {
            if ((EntfernungZumBau + 30) >= Verf�gbareReststrecke())
                return true;
            return false;
        }

        public void LasseNahrungFallen()
        {
            MasterMeise.LasseNahrungFallen();
        }

        public bool KeineWanzenInSicht()
        {
            return AnzahlWanzenInSichtweite <= 0;
        }

        public bool KannNochZuckerTragen()
        {
            return (GetragenesObst == null && AktuelleLast < MaximaleLast);
        }

        public bool KannNochObstTragen()
        {
            return GetragenesObst == null;// && AktuelleLast == 0;
        }

        public bool AufDemWegZuNahrung()
        {
            return Ziel is Nahrung;
        }

        public bool HatSchonWasVor()
        {
            if (Ziel == null)
                return false;
            
            return !MussZumBauSonstVerhungert();
        }

        public void SucheNeuesZiel()
        {
            GeheGeradeaus();
            //GeheGeradeaus(60);
            //DreheUmWinkel(20);
            //GeheGeradeaus(60);
            //DreheUmWinkel(-20);
        }

        public bool IstZielZuWeit(Spielobjekt ziel)
        {
            var entfernung = Koordinate.BestimmeEntfernung(MasterMeise, ziel);
            return (Verf�gbareReststrecke() < entfernung);
        }

        public int Verf�gbareReststrecke()
        {
            return (Reichweite - Zur�ckgelegteStrecke);
        }

        #endregion

        #region Fortbewegung

        public void GeheGeradeaus()
        {
            MasterMeise.GeheGeradeaus();
        }

        public void GeheGeradeaus(int entfernung)
        {
            MasterMeise.GeheGeradeaus(entfernung);
        }

        public void GeheZuBau()
        {
            MasterMeise.GeheZuBau();
        }
        
        public void GeheZuZiel(Spielobjekt ziel)
        {
            MasterMeise.GeheZuZiel(ziel);
        }

        public void GeheWegVon(Spielobjekt ziel, int entfernung = 30)
        {
            MasterMeise.GeheWegVon(ziel);
        }

        public void BeibStehen()
        {
            MasterMeise.BleibStehen();
        }

        public bool BrauchtNochTr�ger(Obst obst)
        {
            return MasterMeise.BrauchtNochTr�ger(obst);
        }

        public void Nimm(Nahrung nahrung)
        {
            MasterMeise.Nimm(nahrung);
        }

        public void Denke(string nachricht)
        {
            MasterMeise.Denke(nachricht);
        }

        public void DreheInRichtung(int richtung)
        {
            MasterMeise.DreheInRichtung(richtung);
        }

        public void Spr�heMarkierung(Markierungen markierung, MarkierungsGr��e gr��e)
        {
            Spr�heMarkierung(markierung, (int)gr��e);
        }

        public void Spr�heMarkierung(Markierungen markierung, int gr��e = 30)
        {
            MasterMeise.Spr�heMarkierung((int)markierung, gr��e);
        }
        
        public void GreifeAn(Insekt ziel)
        {
            MasterMeise.GreifeAn(ziel);
        }

        public void DreheUm()
        {
            MasterMeise.DreheUm();
        }

        public void DreheUmWinkel(int winkel)
        {
            MasterMeise.DreheUmWinkel(winkel);
        }

        public void DreheZuZiel(Spielobjekt ziel)
        {
            MasterMeise.DreheZuZiel(ziel);
        }
        
        /// <summary>
        /// Wenn die Ameise keinerlei Auftr�ge hat, wartet sie auf neue Aufgaben. Um dir das 
        /// mitzuteilen, wird diese Methode hier aufgerufen.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:Wartet
        /// </summary>
        public abstract void Wartet();

        /// <summary>
        /// Erreicht eine Ameise ein drittel ihrer Laufreichweite, wird diese Methode aufgerufen.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:WirdM%C3%BCde
        /// </summary>
        public abstract void WirdM�de();

        /// <summary>
        /// Wenn eine Ameise stirbt, wird diese Methode aufgerufen. Man erf�hrt dadurch, wie 
        /// die Ameise gestorben ist. Die Ameise kann zu diesem Zeitpunkt aber keinerlei Aktion 
        /// mehr ausf�hren.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:IstGestorben
        /// </summary>
        /// <param name="todesart">Art des Todes</param>
        public abstract void IstGestorben(Todesart todesart);

        /// <summary>
        /// Diese Methode wird in jeder Simulationsrunde aufgerufen - ungeachtet von zus�tzlichen 
        /// Bedingungen. Dies eignet sich f�r Aktionen, die unter Bedingungen ausgef�hrt werden 
        /// sollen, die von den anderen Methoden nicht behandelt werden.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:Tick
        /// </summary>
        public abstract void Tick();

        #endregion

        #region Nahrung

        /// <summary>
        /// Sobald eine Ameise innerhalb ihres Sichtradius einen Apfel ersp�ht wird 
        /// diese Methode aufgerufen. Als Parameter kommt das betroffene St�ck Obst.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:Sieht(Obst)"
        /// </summary>
        /// <param name="obst">Das gesichtete St�ck Obst</param>
        public abstract void Sieht(Obst obst);

        /// <summary>
        /// Sobald eine Ameise innerhalb ihres Sichtradius einen Zuckerh�gel ersp�ht wird 
        /// diese Methode aufgerufen. Als Parameter kommt der betroffene Zuckergh�gel.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:Sieht(Zucker)"
        /// </summary>
        /// <param name="zucker">Der gesichtete Zuckerh�gel</param>
        public abstract void Sieht(Zucker zucker);

        /// <summary>
        /// Hat die Ameise ein St�ck Obst als Ziel festgelegt, wird diese Methode aufgerufen, 
        /// sobald die Ameise ihr Ziel erreicht hat. Ab jetzt ist die Ameise nahe genug um mit 
        /// dem Ziel zu interagieren.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:ZielErreicht(Obst)"
        /// </summary>
        /// <param name="obst">Das erreichte St�ck Obst</param>
        public abstract void ZielErreicht(Obst obst);

        /// <summary>
        /// Hat die Ameise eine Zuckerh�gel als Ziel festgelegt, wird diese Methode aufgerufen, 
        /// sobald die Ameise ihr Ziel erreicht hat. Ab jetzt ist die Ameise nahe genug um mit 
        /// dem Ziel zu interagieren.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:ZielErreicht(Zucker)"
        /// </summary>
        /// <param name="zucker">Der erreichte Zuckerh�gel</param>
        public abstract void ZielErreicht(Zucker zucker);

        #endregion

        #region Kommunikation

        /// <summary>
        /// Markierungen, die von anderen Ameisen platziert werden, k�nnen von befreundeten Ameisen 
        /// gewittert werden. Diese Methode wird aufgerufen, wenn eine Ameise zum ersten Mal eine 
        /// befreundete Markierung riecht.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:RiechtFreund(Markierung)"
        /// </summary>
        /// <param name="markierung">Die gerochene Markierung</param>
        public abstract void RiechtFreund(Markierung markierung);

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel ersp�hen k�nnen, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus dem eigenen Volk, so 
        /// wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFreund(Ameise)"
        /// </summary>
        /// <param name="ameise">Ersp�hte befreundete Ameise</param>
        public abstract void SiehtFreund(Ameise ameise);

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel ersp�hen k�nnen, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus einem befreundeten Volk 
        /// (V�lker im selben Team), so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtVerb%C3%BCndeten(Ameise)"
        /// </summary>
        /// <param name="ameise">Ersp�hte verb�ndete Ameise</param>
        public abstract void SiehtVerb�ndeten(Ameise ameise);

        #endregion

        #region Kampf

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel ersp�hen k�nnen, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus einem feindlichen Volk, 
        /// so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFeind(Ameise)"
        /// </summary>
        /// <param name="ameise">Ersp�hte feindliche Ameise</param>
        public abstract void SiehtFeind(Ameise ameise);

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel ersp�hen k�nnen, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Wanze, so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFeind(Wanze)"
        /// </summary>
        /// <param name="wanze">Ersp�hte Wanze</param>
        public abstract void SiehtFeind(Wanze wanze);

        /// <summary>
        /// Es kann vorkommen, dass feindliche Lebewesen eine Ameise aktiv angreifen. Sollte 
        /// eine feindliche Ameise angreifen, wird diese Methode hier aufgerufen und die 
        /// Ameise kann entscheiden, wie sie darauf reagieren m�chte.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:WirdAngegriffen(Ameise)"
        /// </summary>
        /// <param name="ameise">Angreifende Ameise</param>
        public abstract void WirdAngegriffen(Ameise ameise);

        /// <summary>
        /// Es kann vorkommen, dass feindliche Lebewesen eine Ameise aktiv angreifen. Sollte 
        /// eine Wanze angreifen, wird diese Methode hier aufgerufen und die Ameise kann 
        /// entscheiden, wie sie darauf reagieren m�chte.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:WirdAngegriffen(Wanze)"
        /// </summary>
        /// <param name="wanze">Angreifende Wanze</param>
        public abstract void WirdAngegriffen(Wanze wanze);

        #endregion
    }
}