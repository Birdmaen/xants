namespace AntMe.Player.XMeisen
{
    public enum Markierungen
    {
        WanzeAngreifen,
        AmeiseAngreifen,
        WanzeInReichweite,
        AmeiseInReichweite,
        ObstInReichweite,
        ObstBrauchtTräger,
        ZuckerInReichweite,
        ZuckerPosition
    }

    public enum MarkierungsGröße
    {
        WanzeAngreifen = 220,
        AmeiseAngreifen = 160,
        WanzeInReichweite = 120,
        AmeiseInReichweite = 120,
        ObstInReichweite = 200,
        ObstSpur = 25,
        ObstBrauchtTräger = 250,
        ZuckerInReichweite = 200,
        ZuckerSpur = 25,
        ZuckerPosition = 1000
    }
}