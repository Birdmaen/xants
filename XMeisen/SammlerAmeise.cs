using System;
using AntMe.Deutsch;

namespace AntMe.Player.XMeisen
{
    public class SammlerAmeise : StandardAmeise
    {
        public SammlerAmeise(XMeisen masterMeise) : base(masterMeise)
        {
        }

        public override void Tick()
        {
            base.Tick();

            // Ameise zieht Obst zum Bau und braucht Helfer
            if (GetragenesObst != null && BrauchtNochTräger(GetragenesObst))
            {
                SprüheMarkierung(Markierungen.ObstBrauchtTräger, MarkierungsGröße.ObstBrauchtTräger);
            }

            // Trägt Zucker
            if (!KannNochObstTragen() && !KannNochZuckerTragen())
            {
                SprüheMarkierung(Markierungen.ZuckerInReichweite, MarkierungsGröße.ZuckerSpur);
            }

        }

        public override void RiechtFreund(Markierung markierung)
        {
            switch ((Markierungen)markierung.Information)
            {
                case Markierungen.ObstBrauchtTräger:
                    if ((!HatSchonWasVor() && !AufDemWegZuNahrung()) && KannNochObstTragen())
                    {
                        GeheZuZiel(markierung);
                    }
                    break;

                case Markierungen.ObstInReichweite:
                    if ((!HatSchonWasVor() && !AufDemWegZuNahrung()) && KannNochObstTragen())
                    {
                        GeheZuZiel(markierung);
                    }
                    break;

                case Markierungen.ZuckerInReichweite:
                    if ((!HatSchonWasVor() && !AufDemWegZuNahrung()) && KannNochZuckerTragen())
                    {
                        GeheZuZiel(markierung);
                    }
                    break;
                case Markierungen.WanzeAngreifen:
                    break;
                case Markierungen.AmeiseAngreifen:
                    break;
                case Markierungen.WanzeInReichweite:
                    break;
                case Markierungen.AmeiseInReichweite:
                    break;
                case Markierungen.ZuckerPosition:
                    if ((!HatSchonWasVor() && !AufDemWegZuNahrung()) && KannNochZuckerTragen())
                    {
                        if(!IstZielZuWeit(markierung))
                            GeheZuZiel(markierung);
                    }
                    break;
            }
        }

        public override void Sieht(Obst obst)
        {
            SprüheMarkierung(Markierungen.ObstInReichweite, MarkierungsGröße.ObstInReichweite);
            if (KannNochObstTragen() && BrauchtNochTräger(obst) && KeineWanzenInSicht())
            {
                SprüheMarkierung(Markierungen.ObstInReichweite, MarkierungsGröße.ObstSpur);
                GeheZuZiel(obst);
            }
        }

        public override void Sieht(Zucker zucker)
        {
            SprüheMarkierung(Markierungen.ZuckerInReichweite, MarkierungsGröße.ZuckerInReichweite);
            if (KannNochZuckerTragen())
            {
                GeheZuZiel(zucker);
            }
        }

        public override void ZielErreicht(Obst obst)
        {
            SprüheMarkierung(Markierungen.ObstInReichweite, MarkierungsGröße.ObstInReichweite);

            if (KannNochObstTragen() && BrauchtNochTräger(obst) && KeineWanzenInSicht())
            {
                if (BrauchtNochTräger(obst))
                    SprüheMarkierung(Markierungen.ObstBrauchtTräger, MarkierungsGröße.ObstBrauchtTräger);
                Nimm(obst);
                GeheZuBau();
            }
            else
            {
                SucheNeuesZiel();
            }
        }

        public override void ZielErreicht(Zucker zucker)
        {
            SprüheMarkierung(Markierungen.ZuckerInReichweite, MarkierungsGröße.ZuckerInReichweite);
            SprüheMarkierung(Markierungen.ZuckerPosition, MarkierungsGröße.ZuckerPosition);

            if (KannNochZuckerTragen())
            {
                Nimm(zucker);
                GeheZuBau();
            }
            else
            {
                SucheNeuesZiel();
            }
        }

        public override void SiehtFeind(Wanze wanze)
        {
            SprüheMarkierung(Markierungen.WanzeInReichweite, MarkierungsGröße.WanzeInReichweite);

            LasseNahrungFallen();
            GeheWegVon(wanze, 25);
        }
    }
}