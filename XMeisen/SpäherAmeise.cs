using AntMe.Deutsch;

namespace AntMe.Player.XMeisen
{
    public class SpäherAmeise : StandardAmeise
    {
        public SpäherAmeise(XMeisen masterMeise) : base(masterMeise)
        {
        }

        public override void Sieht(Obst obst)
        {
            SprüheMarkierung(Markierungen.ObstInReichweite, MarkierungsGröße.ObstInReichweite);
        }

        public override void Sieht(Zucker zucker)
        {
            SprüheMarkierung(Markierungen.ZuckerInReichweite, MarkierungsGröße.ZuckerInReichweite);
        }
    }
}