﻿using System;
using AntMe.English;

namespace AntMe.Player.XtremeAnts
{
    public class WarriorAnt : AntBase
    {
        private Insect _currentTarget;

        public Insect CurrentTargetInsect
        {
            get
            {
                if(_currentTarget == null)
                    return _currentTarget;

                // If the enemy geths defeated, remove the target
                if (_currentTarget.CurrentEnergy == 0)
                    _currentTarget = null;

                return _currentTarget; 
                
            }
            set { _currentTarget = value; }
        }


        public WarriorAnt(XtremeAntsClass masterAnt) : base(masterAnt)
        {

        }

        public override void ExecuteTask<TTask>(TTask taskToDo)
        {
            CurrentTask = taskToDo;

            CurrentTarget = CurrentTask.Target;
        }

        public override void Waiting()
        {
            if (CurrentTask == null || CurrentTask.HasBeenHandled())
            {
                UnassignFromCurrentTask();
            }
            else
            {
                if (CurrentTask.Target != null && CurrentTask.Target is Insect)
                {
                    if (GetDistanceToTargetWithWayBack(this, CurrentTask.Target) < AvailableDistance())
                    {
                        KillEnemy((Insect) CurrentTask.Target);
                    }
                    else
                        UnassignFromCurrentTask();
                }
                else if (CurrentTask.Target != null && CurrentTask.Target is Fruit)
                {
                    
                }
                else
                    UnassignFromCurrentTask();
            }
        }

        private void KillEnemy(Insect enemy)
        {
            if (enemy is Bug && CurrentTask.NumberOfWorkersAssigned < 6)
                return;

            // Set as target so it will go there, futher handling in SpotsEnemy
            Attack(enemy);
        }

        public override void GettingTired()
        {
        }

        public override void HasDied(KindOfDeath kindOfDeath)
        {
            base.OnAntDied(kindOfDeath);

            if (kindOfDeath == KindOfDeath.Starved)
            {
                MasterAnt.Think("I starved :(");
            } else if(kindOfDeath == KindOfDeath.Eaten)
                MasterAnt.Think("An ant killed me :o");
            else if(kindOfDeath == KindOfDeath.Beaten)
                MasterAnt.Think("Lost 1 on 1");
        }

        public override void Tick()
        {
            base.Tick();

            // Prevent ant from starving
            if (MustGoToAntHill())
                GoToAnthill();

            if (CurrentTask != null && CurrentTask.HasBeenHandled())
                UnassignFromCurrentTask();            
        }

        public override void Spots(Fruit fruit)
        {
            // Report the found to the controller ant
            base.Spots(fruit);
        }

        public override void Spots(Sugar sugar)
        {
            // Report the found to the controller ant
            base.Spots(sugar);
        }

        public override void DestinationReached(Fruit fruit)
        {
            base.DestinationReached(fruit);
        }

        public override void DestinationReached(Sugar sugar)
        {
            base.DestinationReached(sugar);
        }

        protected void UnassignFromCurrentTask()
        {
            CurrentTask?.RemoveWorker(this);
            CurrentTask = null;
            CurrentTarget = null;
            CurrentTargetInsect = null;

            OnIdled();
            SearchNewTarget();
        }

        protected void SearchNewTarget()
        {
            if (MustGoToAntHill())
            {
                GoToAnthill();
                return;
            }

            if (IsAtAnthill())
                MasterAnt.TurnAround();
            
            GoForward(50);
        }

        public override void DetectedScentFriend(Marker marker) { }

        public override void SpotsFriend(Ant ant) { }

        public override void SpotsTeammate(Ant ant) { }

        public override void SpotsEnemy(Ant ant)
        {
            base.SpotsEnemy(ant);

            // Living is more important than killing
            if (MustGoToAntHill())
                return;

            // Ant is too strong
            if (!CanKillEnemy(ant))
            {
                CurrentTargetInsect = null;
                UnassignFromCurrentTask();
                return;
            }
            
            if (CurrentTargetInsect == null || GetDistance(this, ant) < GetDistance(this, CurrentTargetInsect))
                CurrentTargetInsect = ant;

            if (CurrentTargetInsect == ant)
            {
                Attack(ant);
                return;
            }
        }

        public override void SpotsEnemy(Bug bug)
        {
            base.SpotsEnemy(bug);

            // No attack of bugs on it's own, only planned by the controller
            if (CurrentTargetInsect == null || CurrentTargetInsect != bug)
                return;

            if (FriendlyAntsFromSameCasteInViewrange > 4)
                Attack(bug);

            // todo: Implement this so the ant follows the target but will not be killed
            //else
            //    Follow(bug);    
        }

        public override void UnderAttack(Ant ant)
        {
            base.UnderAttack(ant);
            
            if (CurrentEnergy < (MaximumEnergy / 2))
            {
                GoToAnthill();
            }
            else
                Attack(ant);
        }

        public override void UnderAttack(Bug bug)
        {
            base.UnderAttack(bug);
            
            if (CurrentEnergy < (MaximumEnergy / 2))
            {
                GoToAnthill();
            }
            else
                Attack(bug);
        }
    }
}