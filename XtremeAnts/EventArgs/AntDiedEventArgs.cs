﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.EventArgs
{
    public class AntDiedEventArgs : System.EventArgs
    {
        public KindOfDeath KindOfDeath { get; set; }

        public AntDiedEventArgs(KindOfDeath kindOfDeath) : base()
        {
            KindOfDeath = kindOfDeath;
        }
    }
}