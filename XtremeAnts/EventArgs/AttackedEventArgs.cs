﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.EventArgs
{
    public class AttackedEventArgs : System.EventArgs
    {
        public Insect Attacker { get; set; }

        public AttackedEventArgs(Insect attacker) : base()
        {
            Attacker = attacker;
        }
    }
}