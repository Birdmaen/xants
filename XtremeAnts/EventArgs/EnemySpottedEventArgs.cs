﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.EventArgs
{
    public class EnemySpottedEventArgs : System.EventArgs
    {
        public Insect EnemyInsect { get; set; }

        public EnemySpottedEventArgs(Insect enemyInsect) : base()
        {
            EnemyInsect = enemyInsect;
        }
    }
}