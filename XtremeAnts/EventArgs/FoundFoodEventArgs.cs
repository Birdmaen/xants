﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.EventArgs
{
    public class FoundFoodEventArgs : System.EventArgs
    {
        public Food Food { get; set; }

        public FoundFoodEventArgs(Food food)
        {
            Food = food;
        }
    }
}