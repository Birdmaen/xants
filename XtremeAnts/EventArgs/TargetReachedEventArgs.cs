﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.EventArgs
{
    public class TargetReachedEventArgs : System.EventArgs
    {
        public Item Destination { get; set; }

        public TargetReachedEventArgs(Item destination) : base()
        {
            Destination = destination;
        }
    }
}