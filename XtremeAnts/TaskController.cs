﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AntMe.English;
using AntMe.Player.XtremeAnts.Tasks;

namespace AntMe.Player.XtremeAnts
{
    public class TaskController
    {
        private readonly ControllerAnt _controller;
        private List<Task> _tasks = new List<Task>();

        public TaskController(ControllerAnt controller)
        {
            _controller = controller;
        }

        public void Refresh()
        {
            var taskCount = _tasks.Count;
            ClearTasks();

            var clearedTasks = taskCount - _tasks.Count;
            if (clearedTasks > 0)
                Log($"Tasks {_tasks.Count}. Cleared {clearedTasks}");

            SortTasks();
        }

        private void ClearTasks()
        {
            for (var i = 0; i <_tasks.Count; i++)
            {
                var task = _tasks.ElementAt(i);
                if (task.HasBeenHandled())
                {
                    _tasks.Remove(task);
                    i--;    // go one back because we removed one
                }
            }
        }

        private void SortTasks()
        {
            var tasks = _tasks.ToArray();

            bool sorted;
            do
            {
                sorted = true;

                for (int i = 0; i < tasks.Length - 1; i++)
                {
                    // Tauschen wenn zahl1 größer als zahl 2
                    if (_controller.GetDistanceToAnthill(tasks[i].Target) > _controller.GetDistanceToAnthill(tasks[i + 1].Target))
                    {
                        //zahlen tauschen (nur ein Paar)
                        var temp = tasks[i];
                        tasks[i] = tasks[i + 1];
                        tasks[i + 1] = temp;

                        //nicht sortiert
                        sorted = false;
                    }
                }

            } while (!sorted);
            
            // Zurückgeben der sortieren tasks
            _tasks = tasks.ToList();
        }

        public Guid FindItem(Item item)
        {
            foreach (var task in _tasks)
            {
                if (task.Target == item)
                    return task.Id;
            }
            return Guid.Empty;
        }

        public IEnumerable<TTask> GetTasks<TTask>() where TTask : Task
        {
            foreach (var task in _tasks)
            {
                var searchedTask = task as TTask;

                // Task is not of searched type or simply null, we don't need it.
                if (searchedTask == null)
                    continue;

                //  Skip tasks that have been handled already or that don't need anymore help
                if (searchedTask.HasBeenHandled() || (searchedTask.IsBeingHandled && searchedTask.HasEnoughHelpers()))
                    continue;

                yield return searchedTask;
            }
        }

        public void AddTask(Task task)
        {
            if(!_tasks.Contains(task))
                _tasks.Add(task);
        }

        public void RemoveTask(Task task)
        {
            _tasks.Remove(task);
        }
        
        private static void Log(string message)
        {
            Debug.WriteLine(message);
        }
    }
}