﻿using System;
using AntMe.English;
using AntMe.Player.XtremeAnts.EventArgs;
using AntMe.Player.XtremeAnts.Tasks;

namespace AntMe.Player.XtremeAnts
{
    public abstract class AntBase
    {
        public event EventHandler<AntDiedEventArgs> AntDied;
        public event EventHandler<FoundFoodEventArgs> FoundFood;
        public event EventHandler<EnemySpottedEventArgs> EnemySpotted; 
        public event EventHandler<AttackedEventArgs> Attacked; 
        public event EventHandler<TargetReachedEventArgs> TargetReached;
        public event EventHandler<System.EventArgs> Idled;

        protected Food DroppedFruit;

        public Guid Id { get; set; } = Guid.NewGuid();

        public static ControllerAnt Controller;
        
        /// <summary>
        /// Gets or sets the ants current task.
        /// </summary>
        /// <value>
        /// The current task.
        /// </value>
        public Task CurrentTask { get; set; }

        /// <summary>
        /// Gets or sets the current target of the ant.
        /// </summary>
        /// <value>
        /// The current target.
        /// </value>
        public Item CurrentTarget { get; set; }
        
        public XtremeAntsClass MasterAnt { get; }

        public bool IsAttacking => MasterAnt.Destination is Insect;

        protected AntBase(XtremeAntsClass masterAnt)
        {
            MasterAnt   = masterAnt;
        }

        #region Events

        protected void OnAntDied(KindOfDeath kindOfDeath)
        {
            AntDied?.Invoke(this, new AntDiedEventArgs(kindOfDeath));
        }

        protected void OnFoundFood(Food food)
        {
            FoundFood?.Invoke(this, new FoundFoodEventArgs(food));
        }

        protected void OnEnemySpotted(Insect enemyInsect)
        {
            EnemySpotted?.Invoke(this, new EnemySpottedEventArgs(enemyInsect));
        }

        protected void OnAttacked(Insect attacker)
        {
            Attacked?.Invoke(this, new AttackedEventArgs(attacker));
        }

        protected void OnTargetReached(Item destination)
        {
            TargetReached?.Invoke(this, new TargetReachedEventArgs(destination));
        }

        protected void OnIdled()
        {
            Idled?.Invoke(this, System.EventArgs.Empty);
        }

        #endregion

        public abstract void ExecuteTask<TTask>(TTask task) where TTask : Task;

        public bool IsAtAnthill()
        {
            return DistanceToAnthill == 0;
        }

        public bool HasSomethingImportantToDo()
        {
            if (MustGoToAntHill())
                return true;

            return false;
        }

        /// <summary>
        /// The distance the ant can go.
        /// </summary>
        /// <returns></returns>
        public int AvailableDistance()
        {
            return Range - WalkedRange;
        }

        protected bool CanMakeItToTargetAndBack(Item target)
        {
            return CanMakeItToTargetAndBack(this, target);
        }

        protected bool CanMakeItToTargetAndBack(AntBase ant, Item target)
        {
            return (GetDistanceToTargetWithWayBack(this, CurrentTask.Target) < AvailableDistance());
        }

        /// <summary>
        /// Determines if the ant is starving if it doesn't go back to the anthill.
        /// </summary>
        /// <returns></returns>
        public bool MustGoToAntHill()
        {
            if ((DistanceToAnthill + 30) >= AvailableDistance())
                return true;

            if (CurrentEnergy < 20)
                return true;

            return false;
        }
        
        public int GetDistance(AntBase ant, Item target)
        {
            return Coordinate.GetDistanceBetween(ant.MasterAnt, target);
        }

        public int GetDistance(Item start, Item target)
        {
            return Coordinate.GetDistanceBetween(start, target);
        }

        public int GetDistanceToAnthill(Item start)
        {
            return Coordinate.GetDistanceBetween(start, Controller.MasterAnt);  // Controller ant will always stay at the anthill so it's our 'anthill'
        }

        public int GetDistanceToAnthill(AntBase start)
        {
            return Coordinate.GetDistanceBetween(start.MasterAnt, Controller.MasterAnt);
        }

        protected int GetDistanceToTargetWithWayBack(AntBase ant, Item target)
        {
            var distance = GetDistance(ant, target);
            return distance + GetDistanceToAnthill(target);
        }

        public bool IsCarryingSomething()
        {
            return (IsCarryingFruit() || IsCarryingSugar());
        }

        /// <summary>
        /// Determines whether this ant is carrying sugar.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this ant is carrying sugar; otherwise, <c>false</c>.
        /// </returns>
        public bool IsCarryingSugar()
        {
            return MasterAnt.CurrentLoad > 0 && !IsCarryingFruit();
        }

        /// <summary>
        /// Determines whether this ant is carrying a fruit.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this ant is carrying a fruit; otherwise, <c>false</c>.
        /// </returns>
        public bool IsCarryingFruit()
        {
            return MasterAnt.CarryingFruit != null;
        }

        public bool CanCarryMore(Item item)
        {
            var fruit = item as Fruit;
            if (fruit != null && !IsCarryingSomething())
                return true;

            var sugar = item as Sugar;
            if (sugar != null && (!IsCarryingSomething() || IsCarryingSugar()))
                return true;

            return CanCarryMore();
        }

        public bool CanCarryMore()
        {
            return CurrentLoad < MaximumLoad;
        }

        public bool CanKillEnemy(Insect enemy)
        {
            return IsFasterThan(enemy) && IsStrongerThan(enemy);
        }

        /// <summary>
        /// Determines whether this ant is faster than the specified insect.
        /// </summary>
        /// <param name="insect">The insect.</param>
        /// <returns>
        ///   <c>true</c> if is faster than the specified insect; otherwise, <c>false</c>.
        /// </returns>
        protected bool IsFasterThan(Insect insect)
        {
            return (CurrentSpeed > insect.CurrentSpeed);
        }

        /// <summary>
        /// Determines whether is stronger than the specified enemy.
        /// </summary>
        /// <param name="enemy">The enemy.</param>
        /// <returns>
        ///   <c>true</c> if is stronger than the specified enemy; otherwise, <c>false</c>.
        /// </returns>
        protected bool IsStrongerThan(Insect enemy)
        {
            return (Strength >= enemy.AttackStrength);
        }

        protected bool EnemyAntIsCarryingSomething(Ant ant)
        {
            return (ant.CarriedFruit == null || ant.CurrentLoad < 0);
        }

        #region Methods mapping to UberAnts base

        public void GoForward() => MasterAnt.GoForward();

        public void GoForward(int steps) => MasterAnt.GoForward(steps);

        public void TurnByDegrees(int degree) => MasterAnt.TurnByDegrees(degree);

        public void Attack(Insect enemy) => MasterAnt.Attack(enemy);

        public void Stop() => MasterAnt.Stop();

        public bool NeedsCarrier(Fruit fruit) => MasterAnt.NeedsCarrier(fruit);

        public Food DropCurrentItem()
        {
            Food droppedFood = null;
            if (IsCarryingFruit())
                droppedFood = MasterAnt.CarryingFruit;

            MasterAnt.Drop();

            return droppedFood;
        } 

        public void Take(Food food)
        {
            DroppedFruit = null;
            MasterAnt.Take(food);
        }

        public void GoToAnthill()
        {
            MasterAnt.GoToAnthill();
            CurrentTarget = MasterAnt.Destination;
        }
        
        public void GoToDestination(Item destination) => MasterAnt.GoToDestination(destination);

        #endregion

        #region Properties mapping to UberAnts base
        
        /// <summary>
        /// Gets the caste.
        /// </summary>
        /// <value>
        /// The caste.
        /// </value>
        public string Caste => MasterAnt.Caste;
        /// <summary>
        /// Gets the carrying fruit.
        /// </summary>
        /// <value>
        /// The carrying fruit.
        /// </value>
        public Fruit CarryingFruit => MasterAnt.CarryingFruit;
        /// <summary>
        /// Gets the destination.
        /// </summary>
        /// <value>
        /// The destination.
        /// </value>
        public Item Destination => MasterAnt.Destination;
        
        /// <summary>
        /// Gets a value indicating whether this instance is tired.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is tired; otherwise, <c>false</c>.
        /// </value>
        public bool IsTired => MustGoToAntHill();
        
        /// <summary>
        /// Gets a value indicating whether [reached destination].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [reached destination]; otherwise, <c>false</c>.
        /// </value>
        public bool ReachedDestination => MasterAnt.ReachedDestination;
        /// <summary>
        /// Gets the rotation speed.
        /// </summary>
        /// <value>
        /// The rotation speed.
        /// </value>
        public int RotationSpeed => MasterAnt.RotationSpeed;

        /// <summary>
        /// Gets the strength.
        /// </summary>
        /// <value>
        /// The strength.
        /// </value>
        public int Strength => MasterAnt.Strength;
        /// <summary>
        /// Gets the range.
        /// </summary>
        /// <value>
        /// The range.
        /// </value>
        public int Range => MasterAnt.Range;
        /// <summary>
        /// Gets the viewrange.
        /// </summary>
        /// <value>
        /// The viewrange.
        /// </value>
        public int Viewrange => MasterAnt.Viewrange;
        /// <summary>
        /// Gets the distance to anthill.
        /// </summary>
        /// <value>
        /// The distance to anthill.
        /// </value>
        public int DistanceToAnthill => MasterAnt.DistanceToAnthill;
        /// <summary>
        /// Gets the zurückgelegte strecke.
        /// </summary>
        /// <value>
        /// The zurückgelegte strecke.
        /// </value>
        public int WalkedRange => MasterAnt.WalkedRange;

        /// <summary>
        /// Gets the current load.
        /// </summary>
        /// <value>
        /// The current load.
        /// </value>
        public int CurrentLoad => MasterAnt.CurrentLoad;
        /// <summary>
        /// Gets the maximum load.
        /// </summary>
        /// <value>
        /// The maximum load.
        /// </value>
        public int MaximumLoad => MasterAnt.MaximumLoad;
        /// <summary>
        /// Gets the current energy.
        /// </summary>
        /// <value>
        /// The current energy.
        /// </value>
        public int CurrentEnergy => MasterAnt.CurrentEnergy;
        /// <summary>
        /// Gets the maximum energy.
        /// </summary>
        /// <value>
        /// The maximum energy.
        /// </value>
        public int MaximumEnergy => MasterAnt.MaximumEnergy;
        /// <summary>
        /// Gets the current speed.
        /// </summary>
        /// <value>
        /// The current speed.
        /// </value>
        public int CurrentSpeed => MasterAnt.CurrentSpeed;
        /// <summary>
        /// Gets the maximum speed.
        /// </summary>
        /// <value>
        /// The maximum speed.
        /// </value>
        public int MaximumSpeed => MasterAnt.MaximumSpeed;

        /// <summary>
        /// Gets the friendly ants from same caste in viewrange.
        /// </summary>
        /// <value>
        /// The friendly ants from same caste in viewrange.
        /// </value>
        public int FriendlyAntsFromSameCasteInViewrange => MasterAnt.FriendlyAntsFromSameCasteInViewrange;
        /// <summary>
        /// Gets the team ants in viewrange.
        /// </summary>
        /// <value>
        /// The team ants in viewrange.
        /// </value>
        public int TeamAntsInViewrange => MasterAnt.TeamAntsInViewrange;
        /// <summary>
        /// Gets the friendly ants in viewrange.
        /// </summary>
        /// <value>
        /// The friendly ants in viewrange.
        /// </value>
        public int FriendlyAntsInViewrange => MasterAnt.FriendlyAntsInViewrange;
        /// <summary>
        /// Gets the foreign ants in viewrange.
        /// </summary>
        /// <value>
        /// The foreign ants in viewrange.
        /// </value>
        public int ForeignAntsInViewrange => MasterAnt.ForeignAntsInViewrange;
        /// <summary>
        /// Gets the bugs in viewrange.
        /// </summary>
        /// <value>
        /// The bugs in viewrange.
        /// </value>
        public int BugsInViewrange => MasterAnt.BugsInViewrange;

        #endregion
        
        #region Movement

        /// <summary>
        /// If the ant has no assigned tasks, it waits for new tasks. This method 
        /// is called to inform you that it is waiting.
        /// Read more: "http://wiki.antme.net/en/API1:Waiting"
        /// </summary>
        public abstract void Waiting();

        /// <summary>
        /// This method is called when an ant has travelled one third of its 
        /// movement range.
        /// Read more: "http://wiki.antme.net/en/API1:GettingTired"
        /// </summary>
        public abstract void GettingTired();

        /// <summary>
        /// This method is called if an ant dies. It informs you that the ant has 
        /// died. The ant cannot undertake any more actions from that point forward.
        /// Read more: "http://wiki.antme.net/en/API1:HasDied"
        /// </summary>
        /// <param name="kindOfDeath">Kind of Death</param>
        public virtual void HasDied(KindOfDeath kindOfDeath)
        {
            OnAntDied(kindOfDeath);
        }

        /// <summary>
        /// This method is called in every simulation round, regardless of additional 
        /// conditions. It is ideal for actions that must be executed but that are not 
        /// addressed by other methods.
        /// Read more: "http://wiki.antme.net/en/API1:Tick"
        /// </summary>
        public virtual void Tick()
        {
            //if (CurrentTarget is Anthill && ReachedDestination)
            //{
            //    CurrentItem = null;
            //}
        }

        #endregion

        #region Food

        /// <summary>
        /// This method is called as soon as an ant sees an apple within its 360° 
        /// visual range. The parameter is the piece of fruit that the ant has spotted.
        /// Read more: "http://wiki.antme.net/en/API1:Spots(Fruit)"
        /// </summary>
        /// <param name="fruit">spotted fruit</param>
        public virtual void Spots(Fruit fruit)
        {
            OnFoundFood(fruit);
        }

        /// <summary>
        /// This method is called as soon as an ant sees a mound of sugar in its 360° 
        /// visual range. The parameter is the mound of sugar that the ant has spotted.
        /// Read more: "http://wiki.antme.net/en/API1:Spots(Sugar)"
        /// </summary>
        /// <param name="sugar">spotted sugar</param>
        public virtual void Spots(Sugar sugar)
        {
            OnFoundFood(sugar);
        }

        /// <summary>
        /// If the ant’s destination is a piece of fruit, this method is called as soon 
        /// as the ant reaches its destination. It means that the ant is now near enough 
        /// to its destination/target to interact with it.
        /// Read more: "http://wiki.antme.net/en/API1:DestinationReached(Fruit)"
        /// </summary>
        /// <param name="fruit">reached fruit</param>
        public virtual void DestinationReached(Fruit fruit)
        {
            OnTargetReached(fruit);
        }

        /// <summary>
        /// If the ant’s destination is a mound of sugar, this method is called as soon 
        /// as the ant has reached its destination. It means that the ant is now near 
        /// enough to its destination/target to interact with it.
        /// Read more: "http://wiki.antme.net/en/API1:DestinationReached(Sugar)"
        /// </summary>
        /// <param name="sugar">reached sugar</param>
        public virtual void DestinationReached(Sugar sugar)
        {
            TargetReached?.Invoke(this, new TargetReachedEventArgs(sugar));
        }

        #endregion

        #region Communication

        /// <summary>
        /// Friendly ants can detect markers left by other ants. This method is called 
        /// when an ant smells a friendly marker for the first time.
        /// Read more: "http://wiki.antme.net/en/API1:DetectedScentFriend(Marker)"
        /// </summary>
        /// <param name="marker">marker</param>
        public abstract void DetectedScentFriend(Marker marker);

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant sees an ant from the 
        /// same colony.
        /// Read more: "http://wiki.antme.net/en/API1:SpotsFriend(Ant)"
        /// </summary>
        /// <param name="ant">spotted ant</param>
        public abstract void SpotsFriend(Ant ant);

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant detects an ant from a 
        /// friendly colony (an ant on the same team).
        /// Read more: "http://wiki.antme.net/en/API1:SpotsTeammate(Ant)"
        /// </summary>
        /// <param name="ant">spotted ant</param>
        public abstract void SpotsTeammate(Ant ant);

        #endregion

        #region Fight

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant detects an ant from an 
        /// enemy colony.
        /// Read more: "http://wiki.antme.net/en/API1:SpotsEnemy(Ant)"
        /// </summary>
        /// <param name="ant">spotted ant</param>
        public virtual void SpotsEnemy(Ant ant)
        {
            // Only add if multiple ants are near
            if(ForeignAntsInViewrange > 3)
                OnEnemySpotted(ant);
        }

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant sees a bug.
        /// Read more: "http://wiki.antme.net/en/API1:SpotsEnemy(Bug)"
        /// </summary>
        /// <param name="bug">spotted bug</param>
        public virtual void SpotsEnemy(Bug bug)
        {
            OnEnemySpotted(bug);
        }

        /// <summary>
        /// Enemy creatures may actively attack the ant. This method is called if an 
        /// enemy ant attacks; the ant can then decide how to react.
        /// Read more: "http://wiki.antme.net/en/API1:UnderAttack(Ant)"
        /// </summary>
        /// <param name="ant">attacking ant</param>
        public virtual void UnderAttack(Ant ant)
        {
            OnAttacked(ant);
        }

        /// <summary>
        /// Enemy creatures may actively attack the ant. This method is called if a 
        /// bug attacks; the ant can decide how to react.
        /// Read more: "http://wiki.antme.net/en/API1:UnderAttack(Bug)"
        /// </summary>
        /// <param name="bug">attacking bug</param>
        public virtual void UnderAttack(Bug bug)
        {
            OnAttacked(bug);
        }

        #endregion
    }
}