﻿using System;
using AntMe.English;

namespace AntMe.Player.XtremeAnts
{
    public class WorkerAnt : AntBase
    {
        public WorkerAnt(XtremeAntsClass masterAnt) : base(masterAnt)
        {

        }

        public override void ExecuteTask<TTask>(TTask taskToDo)
        {
            CurrentTask = taskToDo;

            CurrentTarget = CurrentTask.Target;
        }

        public override void Waiting()
        {
            if (IsCarryingSomething())
            {
                GoToAnthill();
                return;
            }

            if (CurrentTask == null)
            {
                SearchNewTarget();
                return;
            }

            if (!CanMakeItToTargetAndBack(CurrentTask.Target))
            {
                UnassignFromCurrentTask();
                return;
            }

            GoToDestination(CurrentTask.Target);
        }
        
        public override void GettingTired()
        {
        }

        public override void HasDied(KindOfDeath kindOfDeath)
        {
            base.OnAntDied(kindOfDeath);

            if (kindOfDeath == KindOfDeath.Starved)
            {
                MasterAnt.Think("I starved :(");
            }
        }

        public override void Tick()
        {
            base.Tick();
            

            // Prevent ant from starving
            if (MustGoToAntHill())
                GoToAnthill();
            
            if(IsCarryingSomething())
                GoToAnthill();
            
            if (CurrentTask == null || CurrentTask.HasBeenHandled())
            {
                UnassignFromCurrentTask();
                return;
            }
        }

        public override void Spots(Fruit fruit)
        {
            // Report the found to the controller ant
            base.Spots(fruit);

            if (!IsAttacking && DroppedFruit == fruit)
            {
                Take(fruit);
            }
        }

        public override void Spots(Sugar sugar)
        {
            // Report the found to the controller ant
            base.Spots(sugar);
        }

        public override void DestinationReached(Fruit fruit)
        {
            base.DestinationReached(fruit);
            
            if (CanCarryMore(fruit))
            {
                Take(fruit);
                GoToAnthill();
            }
            else 
                UnassignFromCurrentTask();

        }

        public override void DestinationReached(Sugar sugar)
        {
            base.DestinationReached(sugar);

            if(!CanCarryMore(sugar))
                UnassignFromCurrentTask();

            if (sugar.Amount > 0)
            {
                Take(sugar);
                GoToAnthill();
            }
            else
                UnassignFromCurrentTask();
        }

        protected void UnassignFromCurrentTask()
        {
            CurrentTask?.RemoveWorker(this);
            CurrentTask = null;
            CurrentTarget = null;

            OnIdled();
            SearchNewTarget();
        }

        protected void SearchNewTarget()
        {
            // Prevent from starving
            if (MustGoToAntHill())
            {
                GoToAnthill();
                return;
            }

            // Prepare to rush to the new target
            if (IsAtAnthill())
                MasterAnt.TurnAround();

            // Bring food back to anthill
            if (IsCarryingSomething())
                GoToAnthill();
            else
            {
                Stop();
                //GoForward();     // Stop and hopefully find or get a new target soon
            }
        }

        public override void DetectedScentFriend(Marker marker) { }

        public override void SpotsFriend(Ant ant) { }

        public override void SpotsTeammate(Ant ant) { }

        public override void SpotsEnemy(Ant ant)
        {
            base.SpotsEnemy(ant);

            if (IsCarryingFruit())
            {
                if (GetDistance(this, ant) < 15 && ant.CarriedFruit == CarryingFruit)
                {
                    DroppedFruit = DropCurrentItem();
                    Attack(ant);
                    //Take(droppedFruit);
                }
            }
        }

        public override void SpotsEnemy(Bug bug)
        {
            base.SpotsEnemy(bug);
        }

        public override void UnderAttack(Ant ant)
        {
            base.UnderAttack(ant);
        }

        public override void UnderAttack(Bug bug)
        {
            base.UnderAttack(bug);
        }
    }
}