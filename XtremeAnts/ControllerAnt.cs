﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AntMe.English;
using AntMe.Player.XtremeAnts.EventArgs;
using AntMe.Player.XtremeAnts.Tasks;

namespace AntMe.Player.XtremeAnts
{
    public class ControllerAnt : AntBase
    {
        private readonly Dictionary<Guid, AntBase> _ants = new Dictionary<Guid, AntBase>();
        private readonly List<Food> _foods = new List<Food>();
        private readonly List<Insect> _enemies = new List<Insect>();
        private readonly List<AntBase> _idleWorkers = new List<AntBase>();
        private readonly List<AntBase> _idleWarriors = new List<AntBase>();
        private readonly TaskController _tasksToDo;

        private const int MaximumNumberOfWorkersPerTask = 50;

        public ControllerAnt(XtremeAntsClass masterAnt) : base(masterAnt)
        {
            Controller = this;
            _tasksToDo = new TaskController(this);
        }

        public override void Tick()
        {
            _tasksToDo.Refresh();

            // handle fruit tasks
            foreach (var task in _tasksToDo.GetTasks<FruitTask>())
            {
                HandleTask(task);
            }

            //SugarTask sugarTask;
            foreach (var task in _tasksToDo.GetTasks<SugarTask>())
            {
                HandleTask(task);
            }
            
            foreach (var task in _tasksToDo.GetTasks<KillEnemyTask>())
            {
                HandleTask(task);
            }
        }

        protected void HandleTask<TTask>(TTask task) where TTask : Task
        {
            task.IsBeingHandled = true;

            while (!task.HasEnoughHelpers())
            {
                AntBase idleAnt;
                // Find helpers for the task
                if (task is KillEnemyTask)
                    idleAnt = GetIdleWarriorAnt(XtremeAntsClass.WarriorCaste, task.Target);
                else
                    idleAnt = GetIdleWorkerAnt(XtremeAntsClass.WorkerCaste, task.Target);

                // If there are no idle ants, break to avoid infinite wait for an idle ant that will not come
                if (idleAnt == null)
                    break;

                //// Arbeiter auf maximal x pro Aufgabe beschränken
                //if (task.NumberOfWorkersAssigned >= MaximumNumberOfWorkersPerTask)
                //    break;

                if(string.Equals(XtremeAntsClass.WorkerCaste, idleAnt.Caste))
                    _idleWorkers.Remove(idleAnt);
                if (string.Equals(XtremeAntsClass.WarriorCaste, idleAnt.Caste))
                    _idleWarriors.Remove(idleAnt);

                task.AssignWorker(idleAnt);
            }
        }

        public AntBase GetIdleWorkerAnt(string caste, Item target)
        {
            return GetNearestIdleAnt(_idleWorkers, caste, target);
        }

        public AntBase GetIdleWarriorAnt(string caste, Item target)
        {
            return GetNearestIdleAnt(_idleWarriors, caste, target);
        }

        private AntBase GetNearestIdleAnt(List<AntBase> ants,  string caste, Item target)
        {
            AntBase lowestDistanceAnt = null;
            int lowestDistance = 0;

            foreach (var ant in ants)
            {
                if (!string.Equals(ant.Caste, caste))
                    continue;

                if (lowestDistance == 0)
                {
                    lowestDistanceAnt = ant;
                    lowestDistance = GetDistance(lowestDistanceAnt, target);
                }
                else
                {
                    var distance = GetDistance(ant, target);
                    if (distance < lowestDistance)
                    {
                        lowestDistanceAnt = ant;
                        lowestDistance = distance;
                    }
                }
            }
            return lowestDistanceAnt;
        }

        public void AddFood(Food food)
        {
            if (_foods.Contains(food))
                return;

            _foods.Add(food);

            // Is food a fruit?
            var fruit = food as Fruit;
            if (fruit != null)
            {
                var task = new FruitTask(fruit);
                _tasksToDo.AddTask(task);
                return;
            }

            // Is food sugar?
            var sugar = food as Sugar;
            if (sugar != null)
            {
                var task = new SugarTask(sugar);
                _tasksToDo.AddTask(task);
                return;
            }

            // WTF is it if it's neither fruit nor sugar???
            throw new NotImplementedException($"Unkown food it's neither fruit nor sugar: {food.GetType()}");
        }

        protected void RemoveFood(Food food)
        {
            _foods.Remove(food);
        }

        protected void AddEnemy(Insect enemy)
        {
            if(enemy is Bug)
                return;

            if (_tasksToDo.FindItem(enemy) != Guid.Empty)
                return;
            
            var task = new KillEnemyTask(enemy);
            _tasksToDo.AddTask(task);
        }

        protected void RemoveEnemy(Insect enemy)
        {
            _enemies.Remove(enemy);
        }

        public void AddAnt(AntBase ant)
        {
            if (ant == null)
                throw new ArgumentNullException("Ant cannot be null");
            
            ant.AntDied       += OnAntDiedEvent;
            ant.FoundFood     += AntOnFoundFood;
            ant.EnemySpotted  += AntOnEnemySpotted;
            ant.Attacked      += AntOnAttacked;
            ant.TargetReached += AntOnTargetReached;
            ant.Idled         += AntOnIdled;

            _ants.Add(ant.Id, ant);
        }

        private void RemoveAnt(AntBase ant)
        {
            _ants.Remove(ant.Id);

            if(string.Equals(XtremeAntsClass.WorkerCaste, ant.Caste))
                _idleWorkers.Remove(ant);

            if(string.Equals(XtremeAntsClass.WarriorCaste, ant.Caste))
                _idleWarriors.Remove(ant);
            
            ant.AntDied       -= OnAntDiedEvent;
            ant.FoundFood     -= AntOnFoundFood;
            ant.EnemySpotted  -= AntOnEnemySpotted;
            ant.Attacked      -= AntOnAttacked;
            ant.TargetReached -= AntOnTargetReached;
            ant.Idled         -= AntOnIdled;
        }

        #region Eventhandlers

        private void AntOnEnemySpotted(object sender, EnemySpottedEventArgs args)
        {
            AddEnemy(args.EnemyInsect);
        }

        private void AntOnFoundFood(object sender, FoundFoodEventArgs args)
        {
            AddFood(args.Food);
        }

        private void OnAntDiedEvent(object sender, AntDiedEventArgs args)
        {
            Log($"Ant died. Reason: {args.KindOfDeath}.");
            RemoveAnt((AntBase)sender);
        }

        private void AntOnAttacked(object sender, AttackedEventArgs attackedEventArgs)
        {
            if(CurrentEnergy < (MaximumEnergy / 2))
                GoToAnthill();
        }

        private void AntOnTargetReached(object sender, TargetReachedEventArgs targetReachedEventArgs)
        {
            
        }

        private void AntOnIdled(object sender, System.EventArgs eventArgs)
        {
            var ant = (AntBase) sender;
            if (string.Equals(XtremeAntsClass.WarriorCaste, ant.Caste))
            {
                if (!_idleWarriors.Contains(ant))
                    _idleWarriors.Add(ant);
            }
            if (string.Equals(XtremeAntsClass.WorkerCaste, ant.Caste))
            {
                if(!_idleWorkers.Contains(ant))
                    _idleWorkers.Add(ant);
            }
        }

        #endregion
        
        private static void Log(string message)
        {
            Debug.WriteLine(message);
        }

        #region AntBase implementation

        public override void Waiting() { }

        public override void GettingTired() { }

        public override void Spots(Sugar sugar) { }

        public override void DestinationReached(Fruit fruit) { }

        public override void DestinationReached(Sugar sugar) { }

        public override void DetectedScentFriend(Marker marker) { }

        public override void SpotsFriend(Ant ant) { }

        public override void SpotsTeammate(Ant ant) { }

        public override void SpotsEnemy(Ant ant) { }

        public override void SpotsEnemy(Bug bug) { }

        public override void UnderAttack(Ant ant) { }

        public override void UnderAttack(Bug bug) { }

        public override void ExecuteTask<TTask>(TTask taskToDo) { }

        #endregion
    }
}