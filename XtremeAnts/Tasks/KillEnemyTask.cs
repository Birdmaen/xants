﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.Tasks
{
    public class KillEnemyTask : Task
    {
        public Insect Enemy { get; set; }

        public KillEnemyTask(Insect enemy) : base()
        {
            Enemy = enemy;
            Target = Enemy;
        }

        public override bool HasEnoughHelpers()
        {
            return (NumberOfWorkersAssigned > 5);
        }

        public override bool HasBeenHandled()
        {
            return Enemy == null || Enemy.CurrentEnergy == 0;
        }
    }
}