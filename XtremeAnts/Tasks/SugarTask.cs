﻿using System.Linq;
using AntMe.English;

namespace AntMe.Player.XtremeAnts.Tasks
{
    public class SugarTask : Task
    {
        protected Sugar Sugar;

        public SugarTask(Sugar sugar) : base()
        {
            Sugar  = sugar;
            Target = Sugar;
        }

        public override bool HasEnoughHelpers()
        {
            var worker = Workers.FirstOrDefault();
            if (worker == null)
                return false;

            // Prevent null division
            if (Sugar.Amount == 0)
                return true;

            var loadToMove = Sugar.Amount;
            var power = GetAvailableCarryingPower();

            // If the load that shall be moved smaller that the available power from the assigned ants, no more workers are required
            if (loadToMove < power)
                return true;

            return false;
        }

        public override bool HasBeenHandled()
        {
            return Sugar.Amount == 0;
        }
    }
}