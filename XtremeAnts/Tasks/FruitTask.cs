﻿using AntMe.English;

namespace AntMe.Player.XtremeAnts.Tasks
{
    public class FruitTask : Task
    {
        private const int MaximumNumberOfWorkersAssignable = 5;

        protected Fruit Fruit;

        public FruitTask(Fruit fruit) : base()
        {
            Fruit  = fruit;
            Target = fruit;
        }

        public override bool HasEnoughHelpers()
        {
            return (Workers.Count >= MaximumNumberOfWorkersAssignable);
        }

        public override bool HasBeenHandled()
        {
            if (Fruit == null)
                return true;

            return Fruit.Amount == 0;
        }
    }
}