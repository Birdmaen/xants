﻿using System;
using System.Collections.Generic;
using AntMe.English;

namespace AntMe.Player.XtremeAnts.Tasks
{
    public abstract class Task
    {
        protected readonly List<AntBase> Workers = new List<AntBase>();

        public Guid Id { get; set; } = Guid.NewGuid();

        public bool IsBeingHandled { get; set; }
        
        public Item Target { get; protected set; }

        public abstract bool HasEnoughHelpers();

        public abstract bool HasBeenHandled();
        
        public int NumberOfWorkersAssigned => Workers.Count;
        
        internal void AssignWorker(AntBase worker)
        {
            Workers.Add(worker);
            worker.ExecuteTask(this);
        }

        internal void RemoveWorker(AntBase worker)
        {
            Workers.Remove(worker);
        }

        protected int GetAvailableCarryingPower()
        {
            var maximumCarriage = 0;
            foreach (var worker in Workers)
            {
                maximumCarriage += (worker.MaximumLoad - worker.CurrentLoad);
            }

            return maximumCarriage;
        }
    }
}