﻿using System;
using AntMe.English;
using AntMe.Player.XtremeAnts.Tasks;

namespace AntMe.Player.XtremeAnts
{
    //public class ScoutAnt : AntBase
    //{
    //    public ScoutAnt(XtremeAntsClass masterAnt) : base(masterAnt)
    //    {

    //    }

    //    protected void SearchNewTarget()
    //    {
    //        if (MustGoToAntHill())
    //        {
    //            GoToAnthill();
    //            return;
    //        }

    //        if (IsAtAnthill())
    //            MasterAnt.TurnAround();
    //        MasterAnt.GoForward();

    //    }

    //    public override void Waiting()
    //    {
    //        SearchNewTarget();

    //        if (IsAtAnthill())
    //        {
    //             raise at ant hill event
    //            SearchNewTarget();
    //        }
    //    }

    //    public override void GettingTired()
    //    {
    //        GoToAnthill();
    //    }

    //    public override void HasDied(KindOfDeath kindOfDeath)
    //    {
    //        base.OnAntDied(kindOfDeath);

    //        if (kindOfDeath == KindOfDeath.Starved)
    //        {
    //            MasterAnt.Think("I starved :(");
    //        }
    //    }

    //    public override void Tick()
    //    {
    //         Prevent ant from starving
    //        if(MustGoToAntHill())
    //            GoToAnthill();
    //    }

    //    public override void Spots(Fruit fruit)
    //    {
    //        base.Spots(fruit);

    //         Send signal

    //        if (Destination == null && 
    //            NeedsCarrier(fruit) && 
    //            CanCarryMore(fruit) &&
    //            !HasSomethingImportantToDo()) 
    //        {
    //             wenn task für Frucht bereits erstellt, Task anfordern, 
    //             ansonsten neuen erstellen und broadcasten
    //            var task = new FruitTask(fruit);
    //            MasterAnt.GoToDestination(fruit);
    //        }
    //    }

    //    public override void Spots(Sugar sugar)
    //    {
    //        base.Spots(sugar);

    //        if (Destination == null && 
    //            CanCarryMore(sugar) &&
    //            !HasSomethingImportantToDo())
    //        {
    //             wenn task für Frucht bereits erstellt, Task anfordern, 
    //             ansonsten neuen erstellen und broadcasten
    //            var task = new GetSugarTask(sugar);
    //            MasterAnt.GoToDestination(sugar);
    //        }
    //    }

    //    public override void DestinationReached(Fruit fruit)
    //    {
    //         Send marker

    //        MasterAnt.Take(fruit);
    //        GoToAnthill();
    //    }

    //    public override void DestinationReached(Sugar sugar)
    //    {
    //         Send marker

    //        MasterAnt.Take(sugar);
    //        GoToAnthill();
    //    }

    //    public override void DetectedScentFriend(Marker marker)
    //    {
    //    }

    //    public override void SpotsFriend(Ant ant)
    //    {

    //    }

    //    public override void SpotsTeammate(Ant ant)
    //    {

    //    }

    //    public override void SpotsEnemy(Ant ant)
    //    {
    //        MasterAnt.GoAwayFrom(ant);
    //    }

    //    public override void SpotsEnemy(Bug bug)
    //    {
    //        MasterAnt.GoAwayFrom(bug);
    //    }

    //    public override void UnderAttack(Ant ant)
    //    {
    //        if (IsCarryingSomething())
    //            DropCurrentItem();

    //        MasterAnt.GoAwayFrom(ant);
    //    }

    //    public override void UnderAttack(Bug bug)
    //    {
    //        MasterAnt.GoAwayFrom(bug);
    //    }

    //    public override void ExecuteTask(Task taskToDo)
    //    {
    //        switch (taskToDo.Kind)
    //        {
    //            case InformationKind.None:
    //                break;
    //            case InformationKind.GetFruit:
    //                break;
    //            case InformationKind.GetSugar:
    //                break;
    //            default:
    //                throw new ArgumentOutOfRangeException();
    //        }
    //    }
    //}
    //}

        
    public class ScoutAnt : AntBase
    {
        public ScoutAnt(XtremeAntsClass masterAnt) : base(masterAnt)
        {

        }

        public override void ExecuteTask<TTask>(TTask taskToDo)
        {

        }

        protected void SearchNewTarget()
        {
            if (MustGoToAntHill())
            {
                GoToAnthill();
                return;
            }

            if (IsAtAnthill())
                MasterAnt.TurnAround();

            var randDegree = new Random().Next(-40, 40);
            MasterAnt.TurnByDegrees(randDegree);
            MasterAnt.GoForward();
        }

        public override void Waiting()
        {
            SearchNewTarget();
        }

        public override void GettingTired()
        {
        }

        public override void HasDied(KindOfDeath kindOfDeath)
        {
            base.OnAntDied(kindOfDeath);

            if (kindOfDeath == KindOfDeath.Starved)
            {
                MasterAnt.Think("I starved :(");
            }
        }

        public override void Tick()
        {
            // Prevent ant from starving
            if (MustGoToAntHill())
                GoToAnthill();
        }

        public override void Spots(Fruit fruit)
        {
            base.Spots(fruit);
        }

        public override void Spots(Sugar sugar)
        {
            base.Spots(sugar);
        }

        public override void DestinationReached(Fruit fruit)
        {
            base.DestinationReached(fruit);
        }

        public override void DestinationReached(Sugar sugar)
        {
            base.DestinationReached(sugar);
        }

        public override void DetectedScentFriend(Marker marker) { }

        public override void SpotsFriend(Ant ant) { }

        public override void SpotsTeammate(Ant ant) { }

        public override void SpotsEnemy(Ant ant)
        {
            base.SpotsEnemy(ant);
        }

        public override void SpotsEnemy(Bug bug)
        {
            base.SpotsEnemy(bug);
        }

        public override void UnderAttack(Ant ant)
        {
            base.UnderAttack(ant);

            if (ant.MaximumSpeed > this.CurrentSpeed)
            {
                
                if (IsCarryingSomething())
                    DropCurrentItem();
            }

            MasterAnt.GoAwayFrom(ant);
        }

        public override void UnderAttack(Bug bug)
        {
            base.UnderAttack(bug);
        }
    }
}