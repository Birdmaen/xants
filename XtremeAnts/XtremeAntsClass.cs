using System.Collections.Generic;
using System.Diagnostics;
using AntMe.English;

namespace AntMe.Player.XtremeAnts
{
    [Player(
        ColonyName = "XtremeAnts",
        FirstName  = "Steffen",
        LastName   = "Vogel"
    )]
    [Caste(
        Name = "Controller",
        AttackModifier = 2,
        EnergyModifier = 2,
        LoadModifier = -1,
        RangeModifier = -1,
        RotationSpeedModifier = -1,
        SpeedModifier = -1,
        ViewRangeModifier = -1
    )]
    [Caste(
        Name = "Scout",
        AttackModifier = -1,
        EnergyModifier = -1,
        LoadModifier = -1,
        RangeModifier = 1,
        RotationSpeedModifier = -1,
        SpeedModifier = 1,
        ViewRangeModifier = 2
    )]
    [Caste(
        Name = "Worker",
        AttackModifier = -1,
        EnergyModifier = -1,
        LoadModifier = 2,
        RangeModifier = 0,
        RotationSpeedModifier = -1,
        SpeedModifier = 2,
        ViewRangeModifier = -1
    )]
    [Caste(
        Name = "Warrior",
        AttackModifier = 2,
        EnergyModifier = -1,
        LoadModifier = -1,
        RangeModifier = 0,
        RotationSpeedModifier = -1,
        SpeedModifier = 2,
        ViewRangeModifier = -1
    )]
    public class XtremeAntsClass : BaseAnt
    {
        public const string ControllerCaste = "Controller";
        public const string ScoutCaste      = "Scout";
        public const string WorkerCaste     = "Worker";
        public const string WarriorCaste    = "Warrior";
        
        private static ControllerAnt _controller;

        private AntBase _baseAnt;

        public XtremeAntsClass()
        {
        }

        #region Caste
        
        /// <summary>
        /// Every time that a new ant is born, its job group must be set. You can 
        /// do so with the help of the value returned by this method.
        /// Read more: "http://wiki.antme.net/en/API1:ChooseCaste"
        /// </summary>
        /// <param name="typeCount">Number of ants for every caste</param>
        /// <returns>Caste-Name for the next ant</returns>
        public override string ChooseCaste(Dictionary<string, int> typeCount)
        {
            var numberOfControllers = typeCount.ContainsKey(ControllerCaste)   ? typeCount[ControllerCaste]   : 0;
            var numberOfScouts      = typeCount.ContainsKey(ScoutCaste)   ? typeCount[ScoutCaste]   : 0;
            var numberOfWorkers     = typeCount.ContainsKey(WorkerCaste)  ? typeCount[WorkerCaste]  : 0;
            var numberOfWarriors    = typeCount.ContainsKey(WarriorCaste) ? typeCount[WarriorCaste] : 0;

            var numberOfAnts = numberOfControllers + numberOfScouts + numberOfWorkers + numberOfWarriors;
            Debug.WriteLine($"There are {numberOfAnts} ants currently on the map.");

            string caste;
            
            // Spawn controller
            if (numberOfControllers < 1)
            {
                // Controller
                if(_controller == null)
                    _controller = new ControllerAnt(this);

                _baseAnt = _controller;
                caste = ControllerCaste;
            }
            // Spawn Scouts
            else if (numberOfScouts < 8)
            {
                // Scout
                _baseAnt = new ScoutAnt(this);
                caste = ScoutCaste;
            }
            // Spawn warriors
            //else if (numberOfWarriors < 10)
            //{
            //    // Warrior
            //    _baseAnt = new WarriorAnt(this);
            //    caste = WarriorCaste;
            //}
            // Spawn workers
            else
            {
                // Worker
                _baseAnt = new WorkerAnt(this);
                caste = WorkerCaste;
            }

            if (!(_baseAnt is ControllerAnt))
                _controller.AddAnt(_baseAnt);

            return caste;
        }

        #endregion

        #region Movement

        /// <summary>
        /// If the ant has no assigned tasks, it waits for new tasks. This method 
        /// is called to inform you that it is waiting.
        /// Read more: "http://wiki.antme.net/en/API1:Waiting"
        /// </summary>
        public override void Waiting()
        {
            _baseAnt.Waiting();
        }

        /// <summary>
        /// This method is called when an ant has travelled one third of its 
        /// movement range.
        /// Read more: "http://wiki.antme.net/en/API1:GettingTired"
        /// </summary>
        public override void GettingTired()
        {
            _baseAnt.GettingTired();
        }

        /// <summary>
        /// This method is called if an ant dies. It informs you that the ant has 
        /// died. The ant cannot undertake any more actions from that point forward.
        /// Read more: "http://wiki.antme.net/en/API1:HasDied"
        /// </summary>
        /// <param name="kindOfDeath">Kind of Death</param>
        public override void HasDied(KindOfDeath kindOfDeath)
        {
            _baseAnt.HasDied(kindOfDeath);
        }

        /// <summary>
        /// This method is called in every simulation round, regardless of additional 
        /// conditions. It is ideal for actions that must be executed but that are not 
        /// addressed by other methods.
        /// Read more: "http://wiki.antme.net/en/API1:Tick"
        /// </summary>
        public override void Tick()
        {
            _baseAnt.Tick();
        }

        #endregion

        #region Food

        /// <summary>
        /// This method is called as soon as an ant sees an apple within its 360° 
        /// visual range. The parameter is the piece of fruit that the ant has spotted.
        /// Read more: "http://wiki.antme.net/en/API1:Spots(Fruit)"
        /// </summary>
        /// <param name="fruit">spotted fruit</param>
        public override void Spots(Fruit fruit)
        {
            _baseAnt.Spots(fruit);
        }

        /// <summary>
        /// This method is called as soon as an ant sees a mound of sugar in its 360° 
        /// visual range. The parameter is the mound of sugar that the ant has spotted.
        /// Read more: "http://wiki.antme.net/en/API1:Spots(Sugar)"
        /// </summary>
        /// <param name="sugar">spotted sugar</param>
        public override void Spots(Sugar sugar)
        {
            _baseAnt.Spots(sugar);
        }

        /// <summary>
        /// If the ant’s destination is a piece of fruit, this method is called as soon 
        /// as the ant reaches its destination. It means that the ant is now near enough 
        /// to its destination/target to interact with it.
        /// Read more: "http://wiki.antme.net/en/API1:DestinationReached(Fruit)"
        /// </summary>
        /// <param name="fruit">reached fruit</param>
        public override void DestinationReached(Fruit fruit)
        {
            _baseAnt.DestinationReached(fruit);
        }

        /// <summary>
        /// If the ant’s destination is a mound of sugar, this method is called as soon 
        /// as the ant has reached its destination. It means that the ant is now near 
        /// enough to its destination/target to interact with it.
        /// Read more: "http://wiki.antme.net/en/API1:DestinationReached(Sugar)"
        /// </summary>
        /// <param name="sugar">reached sugar</param>
        public override void DestinationReached(Sugar sugar)
        {
            _baseAnt.DestinationReached(sugar);
        }

        #endregion

        #region Communication

        /// <summary>
        /// Friendly ants can detect markers left by other ants. This method is called 
        /// when an ant smells a friendly marker for the first time.
        /// Read more: "http://wiki.antme.net/en/API1:DetectedScentFriend(Marker)"
        /// </summary>
        /// <param name="marker">marker</param>
        public override void DetectedScentFriend(Marker marker)
        {
            _baseAnt.DetectedScentFriend(marker);
        }

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant sees an ant from the 
        /// same colony.
        /// Read more: "http://wiki.antme.net/en/API1:SpotsFriend(Ant)"
        /// </summary>
        /// <param name="ant">spotted ant</param>
        public override void SpotsFriend(Ant ant)
        {
            _baseAnt.SpotsFriend(ant);
        }

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant detects an ant from a 
        /// friendly colony (an ant on the same team).
        /// Read more: "http://wiki.antme.net/en/API1:SpotsTeammate(Ant)"
        /// </summary>
        /// <param name="ant">spotted ant</param>
        public override void SpotsTeammate(Ant ant)
        {
            _baseAnt.SpotsTeammate(ant);
        }

        #endregion

        #region Fight

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant detects an ant from an 
        /// enemy colony.
        /// Read more: "http://wiki.antme.net/en/API1:SpotsEnemy(Ant)"
        /// </summary>
        /// <param name="ant">spotted ant</param>
        public override void SpotsEnemy(Ant ant)
        {
            _baseAnt.SpotsEnemy(ant);
        }

        /// <summary>
        /// Just as ants can see various types of food, they can also visually detect 
        /// other game elements. This method is called if the ant sees a bug.
        /// Read more: "http://wiki.antme.net/en/API1:SpotsEnemy(Bug)"
        /// </summary>
        /// <param name="bug">spotted bug</param>
        public override void SpotsEnemy(Bug bug)
        {
            _baseAnt.SpotsEnemy(bug);
        }

        /// <summary>
        /// Enemy creatures may actively attack the ant. This method is called if an 
        /// enemy ant attacks; the ant can then decide how to react.
        /// Read more: "http://wiki.antme.net/en/API1:UnderAttack(Ant)"
        /// </summary>
        /// <param name="ant">attacking ant</param>
        public override void UnderAttack(Ant ant)
        {
            _baseAnt.UnderAttack(ant);
        }

        /// <summary>
        /// Enemy creatures may actively attack the ant. This method is called if a 
        /// bug attacks; the ant can decide how to react.
        /// Read more: "http://wiki.antme.net/en/API1:UnderAttack(Bug)"
        /// </summary>
        /// <param name="bug">attacking bug</param>
        public override void UnderAttack(Bug bug)
        {
            _baseAnt.UnderAttack(bug);
        }

        #endregion
    }
}